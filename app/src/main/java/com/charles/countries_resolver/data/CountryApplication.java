package com.charles.countries_resolver.data;

import android.app.Application;

/**
 * Allows the storage and access of data common to the entire application
 */
public class CountryApplication extends Application
{

    private static CountryApplication instance;

    private CountryList countryList;

    /**
     * @return The singleton instance of CountryApplication
     */
    public static CountryApplication getInstance()
    {
        return instance;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        instance = this;
    }

    /**
     * Creates a {@link CountryList} which binds the given fields and views
     * @param fieldsToBind The names of the fields of the database to bind from
     * @param viewsToBind The resource identifiers of the views to bind to
     */
    public void createCountryList(String[] fieldsToBind, int[] viewsToBind)
    {
        countryList = new CountryList(fieldsToBind, viewsToBind);
    }

    /**
     * @return The {@link CountryList} stored in the application
     */
    public CountryList getCountryList()
    {
        return countryList;
    }

}