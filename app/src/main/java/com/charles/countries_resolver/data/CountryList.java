package com.charles.countries_resolver.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.widget.SimpleCursorAdapter;
import com.charles.countries_resolver.R;
import com.charles.countries_resolver.view.CountryViewBinder;

/**
 * Allows objects to be constructed which manage a list of countries stored in the countries provider's database
 */
public class CountryList
{

    public static final String COL_ID = "_id";
    public static final String COL_COUNTRY = "Country";
    public static final String COL_CAPITAL = "Capital";
    public static final String COL_POPULATION = "Population";
    public static final String COL_AREA = "Area";
    public static final String COL_POPULATION_DENSITY = "Population_Density";
    public static final String COL_OFFICIAL_LANGUAGES = "Official_Languages";
    public static final String COL_FLAG = "Flag";
    private static final Uri countriesUri = Uri.parse("content://com.charles.countries_provider/");

    private final String[] bindingFields;
    private final SimpleCursorAdapter adapter;
    private String sortingStatement;

    /**
     * Constructs a new CountryList object populated with countries from the Countries Provider. The adapter binds the bindingFields
     * to the bindingViews in order, so for example the first field in bindingFields binds to the first view in bindingViews.
     * @param bindingFields The names of the fields of the database to bind from
     * @param bindingViews The resource identifiers of the views to bind to
     */
    @SuppressWarnings("all")
    CountryList(String[] bindingFields, int[] bindingViews)
    {
        this.bindingFields = bindingFields;
        Cursor cursor = CountryApplication.getInstance().getContentResolver().query(countriesUri, bindingFields, null, null, null);
        adapter = new SimpleCursorAdapter(CountryApplication.getInstance(), R.layout.list_item_country, cursor, bindingFields, bindingViews, 0);
        adapter.setViewBinder(new CountryViewBinder());
        sortingStatement = "_id ASC";
    }

    /**
     * @return The {@link SimpleCursorAdapter} which links fields in the country provider's database to the user interface components
     */
    public SimpleCursorAdapter getAdapter()
    {
        return adapter;
    }

    /**
     * Adds a country to the CountryList with the given information. If the country already exists then the add is not allowed.
     * @param country The name of the country
     * @param capital The capital city of the country
     * @param population The population of the country
     * @param area The area of the country in sq km
     * @param officialLanguages The official languages of the country
     * @param flag An array of bytes representing the image of the country's flag
     * @return Whether it is true or false that the country was allowed to be added
     */
    public boolean addCountry(String country, String capital, long population, long area, String officialLanguages, byte[] flag)
    {
        if(countryExists(country))
        {
            return false;
        }
        ContentValues countryValues = createCountryValues(country, capital, population, area, officialLanguages, flag);
        CountryApplication.getInstance().getContentResolver().insert(countriesUri, countryValues);
        sortCountries();
        return true;
    }

    /**
     * Edits a country in the CountryList with the given new information. If the new country already exists then the edit is not allowed.
     * @param oldCountry The old name of the country used for identification
     * @param country The new name of the country
     * @param capital The new capital city of the country
     * @param population The new population of the country
     * @param area The new area of the country in sq km
     * @param officialLanguages The new official languages of the country
     * @param flag An array of bytes representing the image of the country's new flag
     * @return Whether it is true or false that the country was allowed to be edited
     */
    public boolean editCountry(String oldCountry, String country, String capital, long population, long area, String officialLanguages, byte[] flag)
    {
        if(countryExists(country) && !country.equals(oldCountry))
        {
            return false;
        }
        ContentValues countryValues = createCountryValues(country, capital, population, area, officialLanguages, flag);
        CountryApplication.getInstance().getContentResolver().update(countriesUri, countryValues, COL_COUNTRY + " = '" + oldCountry + "'", null);
        sortCountries();
        return true;
    }

    /**
     * Deletes a country from the CountryList
     * @param country The name of the country to be deleted
     */
    public void deleteCountry(String country)
    {
        CountryApplication.getInstance().getContentResolver().delete(countriesUri, COL_COUNTRY + " = '" + country + "'", null);
        sortCountries();
    }

    /**
     * Resets the countries in the CountryList to the default initial countries
     */
    public void resetCountries()
    {
        CountryApplication.getInstance().getContentResolver().call(countriesUri, "reset", null, null);
        sortCountries();
    }

    /**
     * Clears the CountryList by deleting all the countries
     */
    public void clearCountries()
    {
        CountryApplication.getInstance().getContentResolver().delete(countriesUri, null, null);
        applyUpdateToList();
    }

    /**
     * Updates the current sorting statement with the given parameters then sorts the countries in the CountryList based on this
     * @param sortField The name of the database field by which to sort the countries
     * @param sortOrdering The sort ordering which can be ASC for ascending or DESC for descending
     */
    public void sortCountries(String sortField, String sortOrdering)
    {
        sortingStatement = sortField + " " + sortOrdering;
        sortCountries();
    }

    /**
     * Sorts the countries in the CountryList using the current value of the sorting statement
     */
    private void sortCountries()
    {
        Cursor cursor = CountryApplication.getInstance().getContentResolver().query(countriesUri, bindingFields, null, null, sortingStatement);
        applyUpdateToList(cursor);
    }

    /**
     * Applies any changes to the CountryList by updating the {@link SimpleCursorAdapter} with the current database. This allows the
     * user interface components to be updated accordingly.
     */
    private void applyUpdateToList()
    {
        Cursor cursor = CountryApplication.getInstance().getContentResolver().query(countriesUri, bindingFields, null, null, null);
        applyUpdateToList(cursor);
    }

    /**
     * Applies any changes to the CountryList by updating the {@link SimpleCursorAdapter} with the given Cursor from the current
     * database. This allows the user interface components to be updated accordingly.
     * @param cursor A {@link Cursor} representing the current database
     */
    private void applyUpdateToList(Cursor cursor)
    {
        adapter.changeCursor(cursor);
        adapter.notifyDataSetChanged();
    }

    /**
     * Checks if a country already exists in the CountryList
     * @param country The name of the country to check for existence
     * @return Whether it is true or false that the country exists
     */
    @SuppressWarnings("all")
    private boolean countryExists(String country)
    {
        Cursor cursor = CountryApplication.getInstance().getContentResolver().query(countriesUri, bindingFields, COL_COUNTRY + " = '" + country + "'", null, null);
        return cursor != null && cursor.getCount() > 0;
    }

    /**
     * Creates a {@link ContentValues} with the country information given which is used for updating the database
     * @param country The name of the country
     * @param capital The capital city of the country
     * @param population The population of the country
     * @param area The area of the country in sq km
     * @param officialLanguages The official languages of the country
     * @param flag An array of bytes representing the image of the country's flag
     * @return The {@link ContentValues} containing the information given
     */
    private ContentValues createCountryValues(String country, String capital, long population, long area, String officialLanguages, byte[] flag)
    {
        ContentValues countryValues = new ContentValues();
        countryValues.put(COL_COUNTRY, country);
        countryValues.put(COL_CAPITAL, capital);
        countryValues.put(COL_POPULATION, population);
        countryValues.put(COL_AREA, area);
        countryValues.put(COL_POPULATION_DENSITY, (double)population / (double)area);
        countryValues.put(COL_OFFICIAL_LANGUAGES, officialLanguages);
        countryValues.put(COL_FLAG, flag);
        return countryValues;
    }

}
