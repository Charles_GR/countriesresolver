package com.charles.countries_resolver.view.country_input;

import android.os.Bundle;
import android.view.View;
import android.widget.*;
import com.charles.countries_resolver.R;
import com.charles.countries_resolver.data.CountryApplication;
import com.charles.countries_resolver.data.CountryList;

/**
 * Displays a form which allows the user to input information about a country then add it to the {@link CountryList}
 */
public class AddCountryActivity extends CountryInputActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        setContentView(R.layout.activity_add_country);
        btnAction = (Button)findViewById(R.id.btnAdd);
        super.onCreate(savedInstanceState);
        ivFlag.setVisibility(View.GONE);
        tvNoFlag.setVisibility(View.VISIBLE);
        btnRemoveFlag.setVisibility(View.GONE);
    }

    @Override
    protected boolean performAction(String country, String capital, long population, long area, String officialLanguages, byte[] flag)
    {
        return CountryApplication.getInstance().getCountryList().addCountry(country, capital, population, area, officialLanguages, flag);
    }

}
