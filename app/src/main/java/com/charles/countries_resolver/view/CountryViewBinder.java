package com.charles.countries_resolver.view;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.widget.*;
import android.widget.SimpleCursorAdapter.ViewBinder;
import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Constructs objects which bind fields in the database to views in the {@link ListView} of countries
 */
public class CountryViewBinder implements ViewBinder
{

    @Override
    @SuppressWarnings("all")
    public boolean setViewValue(View view, Cursor cursor, int columnIndex)
    {
        TextView textView = view instanceof TextView ? (TextView)view : null;
        ImageView imageView = view instanceof ImageView ? (ImageView)view : null;
        switch(cursor.getType(columnIndex))
        {
            case Cursor.FIELD_TYPE_INTEGER:
                String text = NumberFormat.getInstance().format(cursor.getLong(columnIndex));
                textView.setText(text);
                break;
            case Cursor.FIELD_TYPE_FLOAT:
                text = DecimalFormat.getInstance().format(Math.round(100d * cursor.getFloat(columnIndex)) / 100d);
                textView.setText(text);
                break;
            case Cursor.FIELD_TYPE_STRING:
                text = cursor.getString(columnIndex);
                textView.setText(text);
                break;
            case Cursor.FIELD_TYPE_BLOB:
                byte[] flagBytes = cursor.getBlob(columnIndex);
                Bitmap flag = BitmapFactory.decodeByteArray(flagBytes, 0, flagBytes.length);
                imageView.setImageBitmap(flag);
                break;
            default:
                return false;
        }
        return true;
    }

}
