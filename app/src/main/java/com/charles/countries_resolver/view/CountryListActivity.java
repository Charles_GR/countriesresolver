package com.charles.countries_resolver.view;

import static com.charles.countries_resolver.data.CountryList.*;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.AdapterView.OnItemSelectedListener;
import com.charles.countries_resolver.R;
import com.charles.countries_resolver.data.CountryApplication;
import com.charles.countries_resolver.data.CountryList;
import com.charles.countries_resolver.processing.LoadStateTask;
import com.charles.countries_resolver.processing.SaveStateTask;
import com.charles.countries_resolver.view.country_input.AddCountryActivity;
import com.charles.countries_resolver.view.country_input.EditCountryActivity;
import java.util.*;

/**
 * Displays a {@link ListView} containing countries and allows the user to perform some common operations to update the {@link CountryList}
 */
public class CountryListActivity extends Activity implements OnClickListener, OnItemSelectedListener
{

    private static final String[] fieldsToBind = {COL_ID, COL_COUNTRY, COL_CAPITAL, COL_POPULATION, COL_AREA, COL_POPULATION_DENSITY, COL_OFFICIAL_LANGUAGES, COL_FLAG};
    private static final int[] viewsToBind = {0, R.id.tvCountryValue, R.id.tvCapitalValue, R.id.tvPopulationValue, R.id.tvAreaValue, R.id.tvPopulationDensityValue, R.id.tvOfficialLanguagesValue, R.id.ivFlag};
    private static final String[] sortFields = {"Insertion", COL_COUNTRY, COL_CAPITAL, COL_POPULATION, COL_AREA, COL_POPULATION_DENSITY.replace("_", " "), COL_OFFICIAL_LANGUAGES.replace("_", " ")};
    private static final String[] sortOrderings = {"Ascending", "Descending"};
    private static CountryListActivity instance;

    private Spinner spnSortField;
    private Spinner spnSortOrdering;
    private ListView lvCountries;
    private Button btnAdd;
    private Button btnEdit;
    private Button btnDelete;
    private Button btnReset;
    private Button btnClear;

    /**
     * @return The singleton instance of CountryListActivity
     */
    public static CountryListActivity getInstance()
    {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        checkCountriesProviderInstalled();
        CountryApplication.getInstance().createCountryList(fieldsToBind, viewsToBind);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_list);
        instance = this;
        spnSortField = (Spinner)findViewById(R.id.spnSortField);
        spnSortOrdering = (Spinner)findViewById(R.id.spnSortOrdering);
        lvCountries = (ListView)findViewById(R.id.lvCountries);
        btnAdd = (Button)findViewById(R.id.btnAdd);
        btnEdit = (Button)findViewById(R.id.btnEdit);
        btnDelete = (Button)findViewById(R.id.btnDelete);
        btnReset = (Button)findViewById(R.id.btnReset);
        btnClear = (Button)findViewById(R.id.btnClear);
        spnSortField.setOnItemSelectedListener(this);
        spnSortField.setAdapter(new ArrayAdapter<>(this, R.layout.spinner_item, sortFields));
        spnSortOrdering.setOnItemSelectedListener(this);
        spnSortOrdering.setAdapter(new ArrayAdapter<>(this, R.layout.spinner_item, sortOrderings));
        lvCountries.setAdapter(CountryApplication.getInstance().getCountryList().getAdapter());
        lvCountries.setSelector(R.drawable.list_item_selector);
        btnAdd.setOnClickListener(this);
        btnEdit.setOnClickListener(this);
        btnDelete.setOnClickListener(this);
        btnReset.setOnClickListener(this);
        btnClear.setOnClickListener(this);
        new LoadStateTask().execute();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        new SaveStateTask().execute(spnSortField.getSelectedItem() == null ? "" : spnSortField.getSelectedItem().toString(),
                spnSortOrdering.getSelectedItem() == null ? "" : spnSortOrdering.getSelectedItem().toString(),
                getCheckedCountries(), lvCountries.getFirstVisiblePosition(),
                (lvCountries.getChildAt(0) == null) ? 0 : lvCountries.getChildAt(0).getTop() - lvCountries.getPaddingTop());
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnAdd:
                startActivity(new Intent(this, AddCountryActivity.class));
                break;
            case R.id.btnEdit:
                editCheckedCountry();
                break;
            case R.id.btnDelete:
                deleteCheckedCountries();
                break;
            case R.id.btnReset:
                CountryApplication.getInstance().getCountryList().resetCountries();
                break;
            case R.id.btnClear:
                CountryApplication.getInstance().getCountryList().clearCountries();
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        if(parent == spnSortField || parent == spnSortOrdering)
        {
            Set<String> checkedCountries = getCheckedCountries();
            String sortField = spnSortField.getSelectedItem().toString().equals("Insertion") ? COL_ID
                    : spnSortField.getSelectedItem().toString().replace(" ", "_");
            String sortOrdering = spnSortOrdering.getSelectedItem().toString().replace("ending", "").toUpperCase();
            CountryApplication.getInstance().getCountryList().sortCountries(sortField, sortOrdering);
            setCheckedCountries(checkedCountries);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {

    }

    /**
     * Updates the state of the user interface according to the preferences given
     * @param sortByField The field of the sort field {@link Spinner} that should be selected
     * @param sortByOrdering The field of the sort ordering {@link Spinner} that should be selected
     * @param checkedCountries The countries that should be set as checked
     * @param listScrollIndex The index of the topmost visible item in the {@link ListView}
     * @param listScrollTop The topmost visible item's offset from the top of the {@link ListView}
     */
    @SuppressWarnings("unchecked")
    public void updateState(String sortByField, String sortByOrdering, Set<String> checkedCountries, int listScrollIndex, int listScrollTop)
    {
        spnSortField.setSelection(((ArrayAdapter<String>)spnSortField.getAdapter()).getPosition(sortByField));
        spnSortOrdering.setSelection(((ArrayAdapter<String>)spnSortOrdering.getAdapter()).getPosition(sortByOrdering));
        setCheckedCountries(checkedCountries);
        lvCountries.setSelectionFromTop(listScrollIndex, listScrollTop);
    }

    /**
     * @return A set of countries in the {@link ListView} that are checked
     */
    private Set<String> getCheckedCountries()
    {
        Set<String> checkedCountries = new HashSet<>();
        for(int i = 0; i < lvCountries.getCount(); i++)
        {
            if(lvCountries.isItemChecked(i))
            {
                Cursor cursor = (Cursor)lvCountries.getItemAtPosition(i);
                String country = cursor.getString(cursor.getColumnIndex(COL_COUNTRY));
                checkedCountries.add(country);
            }
        }
        return checkedCountries;
    }

    /**
     * Sets the countries in the {@link ListView} to checked if they are contained in the given {@link Set} of countries and
     * unchecked otherwise.
     * @param countries The set of countries to set as checked
     */
    private void setCheckedCountries(Set<String> countries)
    {
        for(int i = 0; i < lvCountries.getCount(); i++)
        {
            Cursor cursor = (Cursor)lvCountries.getItemAtPosition(i);
            String country = cursor.getString(cursor.getColumnIndex(COL_COUNTRY));
            lvCountries.setItemChecked(i, countries.contains(country));
        }
    }

    /**
     * Edits a single checked country from the {@link CountryList} by launching {@link EditCountryActivity}
     */
    private void editCheckedCountry()
    {
        if(lvCountries.getCheckedItemCount() != 1)
        {
            Toast.makeText(this, "No single item is selected!", Toast.LENGTH_LONG).show();
            return;
        }
        for(int i = 0; i < lvCountries.getCount(); i++)
        {
            if(lvCountries.isItemChecked(i))
            {
                Cursor cursor = (Cursor)lvCountries.getItemAtPosition(i);
                Intent intent = new Intent(this, EditCountryActivity.class);
                intent.putExtra(COL_COUNTRY, cursor.getString(cursor.getColumnIndex(COL_COUNTRY)));
                intent.putExtra(COL_CAPITAL, cursor.getString(cursor.getColumnIndex(COL_CAPITAL)));
                intent.putExtra(COL_POPULATION, cursor.getLong(cursor.getColumnIndex(COL_POPULATION)));
                intent.putExtra(COL_AREA, cursor.getLong(cursor.getColumnIndex(COL_AREA)));
                intent.putExtra(COL_OFFICIAL_LANGUAGES, cursor.getString(cursor.getColumnIndex(COL_OFFICIAL_LANGUAGES)));
                intent.putExtra(COL_FLAG, cursor.getBlob(cursor.getColumnIndex(COL_FLAG)));
                startActivity(intent);
                return;
            }
        }
    }

    /**
     * Deletes all the checked countries from the {@link CountryList}
     */
    private void deleteCheckedCountries()
    {
        if(lvCountries.getCheckedItemCount() == 0)
        {
            Toast.makeText(this, "No items are selected!", Toast.LENGTH_LONG).show();
            return;
        }
        List<String> countriesToDelete = new ArrayList<>();
        for(int i = 0; i < lvCountries.getCount(); i++)
        {
            if(lvCountries.isItemChecked(i))
            {
                Cursor cursor = (Cursor)lvCountries.getItemAtPosition(i);
                String country = cursor.getString(cursor.getColumnIndex(COL_COUNTRY));
                countriesToDelete.add(country);
            }
        }
        for(String country : countriesToDelete)
        {
            CountryApplication.getInstance().getCountryList().deleteCountry(country);
        }
    }

    /**
     * Checks that the countries provider application is installed and if not finishes the activity
     */
    private void checkCountriesProviderInstalled()
    {
        for(ApplicationInfo appInfo : getPackageManager().getInstalledApplications(0))
        {
            if(appInfo.packageName.equals("com.charles.countries_provider"))
            {
                setTheme(R.style.AppTheme);
                return;
            }
        }
        Toast.makeText(this, "Countries provider is not installed!", Toast.LENGTH_SHORT).show();
        finish();
    }

}