package com.charles.countries_resolver.view.country_input;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.provider.MediaStore.Images.Media;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.charles.countries_resolver.R;
import java.io.ByteArrayOutputStream;

/**
 * Displays a form which allows the user to input information about a country
 */
abstract class CountryInputActivity extends Activity implements OnClickListener
{

    protected static final int REQUEST_IMAGE = 1;

    protected EditText etCountryValue;
    protected EditText etCapitalValue;
    protected EditText etPopulationValue;
    protected EditText etAreaValue;
    protected EditText etOfficialLanguagesValue;
    protected ImageView ivFlag;
    protected TextView tvNoFlag;
    protected Button btnSelectFlag;
    protected Button btnRemoveFlag;
    protected Button btnAction;
    protected Button btnCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        etCountryValue = (EditText)findViewById(R.id.etCountryValue);
        etCapitalValue = (EditText)findViewById(R.id.etCapitalValue);
        etPopulationValue = (EditText)findViewById(R.id.etPopulationValue);
        etAreaValue = (EditText)findViewById(R.id.etAreaValue);
        etOfficialLanguagesValue = (EditText)findViewById(R.id.etOfficialLanguagesValue);
        ivFlag = (ImageView)findViewById(R.id.ivFlag);
        tvNoFlag = (TextView)findViewById(R.id.tvNoFlag);
        btnSelectFlag = (Button)findViewById(R.id.btnSelectFlag);
        btnRemoveFlag = (Button)findViewById(R.id.btnRemoveFlag);
        btnCancel = (Button)findViewById(R.id.btnCancel);
        btnSelectFlag.setOnClickListener(this);
        btnRemoveFlag.setOnClickListener(this);
        btnAction.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnSelectFlag:
                startActivityForResult(new Intent(Intent.ACTION_PICK, Media.EXTERNAL_CONTENT_URI), REQUEST_IMAGE);
                break;
            case R.id.btnRemoveFlag:
                ivFlag.setImageBitmap(null);
                ivFlag.setVisibility(View.GONE);
                tvNoFlag.setVisibility(View.VISIBLE);
                btnRemoveFlag.setVisibility(View.GONE);
                break;
            case R.id.btnAdd:
            case R.id.btnSave:
                submitForm();
                break;
            case R.id.btnCancel:
                finish();
                break;
        }
    }

    @Override
    @SuppressWarnings("all")
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        try
        {
            if(requestCode == REQUEST_IMAGE && resultCode == RESULT_OK && data != null)
            {
                Cursor cursor = getContentResolver().query(data.getData(), new String[]{Media.DATA}, null, null, null);
                cursor.moveToFirst();
                String fileContents = cursor.getString(cursor.getColumnIndex(Media.DATA));
                Bitmap flag = BitmapFactory.decodeFile(fileContents);
                ivFlag.setImageBitmap(flag);
                ivFlag.setVisibility(flag == null ? View.GONE : View.VISIBLE);
                tvNoFlag.setVisibility(flag == null ? View.VISIBLE : View.GONE);
                btnRemoveFlag.setVisibility(flag == null ? View.GONE : View.VISIBLE);
            }
        }
        catch(Exception e)
        {
            Toast.makeText(this, "Error reading the selected image!", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Performs an action with the country information given by the user
     * @param country The name of the country
     * @param capital The capital city of the country
     * @param population The population of the country
     * @param area The area of the country in sq km
     * @param officialLanguages The official languages of the country
     * @param flag An array of bytes representing the image of the country's flag
     */
    protected abstract boolean performAction(String country, String capital, long population, long area, String officialLanguages, byte[] flag);

    /**
     * Converts the flag image inside the {@link ImageView} into an array of bytes
     * @return An array of bytes representing the flag image
     */
    private byte[] getFlagBytes()
    {
        try
        {
            BitmapDrawable bitmapDrawable = (BitmapDrawable)ivFlag.getDrawable();
            Bitmap flag = bitmapDrawable.getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            flag.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            return stream.toByteArray();
        }
        catch(Exception e)
        {
            return new byte[0];
        }
    }

    /**
     * Validates the form inputs then uses them to perform an action with the inputs given
     */
    private void submitForm()
    {
        if(formHasEmptyInputs())
        {
            Toast.makeText(this, "Some form inputs are empty!", Toast.LENGTH_LONG).show();
            return;
        }
        if(Long.parseLong(etAreaValue.getText().toString()) == 0)
        {
            Toast.makeText(this, "The area cannot be zero!", Toast.LENGTH_LONG).show();
            return;
        }
        if(performAction(etCountryValue.getText().toString().trim(), etCapitalValue.getText().toString().trim(),
                Long.parseLong(etPopulationValue.getText().toString()), Long.parseLong(etAreaValue.getText().toString()),
                etOfficialLanguagesValue.getText().toString().trim(), getFlagBytes()))
        {
            finish();
        }
        else
        {
            Toast.makeText(this, "That country already exists!", Toast.LENGTH_LONG).show();
        }
    }


    /**
     * Checks if the form has empty input fields
     * @return Whether it is true or false that the form has empty input fields
     */
    private boolean formHasEmptyInputs()
    {
        boolean empty = etCountryValue.getText().toString().trim().length() == 0;
        empty = empty || etCapitalValue.getText().toString().trim().length() == 0;
        empty = empty || etPopulationValue.getText().toString().trim().length() == 0;
        empty = empty || etAreaValue.getText().toString().trim().length() == 0;
        empty = empty || etOfficialLanguagesValue.getText().toString().trim().length() == 0;
        return empty;
    }

}