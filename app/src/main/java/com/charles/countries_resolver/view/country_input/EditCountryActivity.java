package com.charles.countries_resolver.view.country_input;

import static com.charles.countries_resolver.data.CountryList.*;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.charles.countries_resolver.R;
import com.charles.countries_resolver.data.CountryApplication;
import com.charles.countries_resolver.data.CountryList;

/**
 * Displays a form which allows the user to edit information about a country inside the {@link CountryList}
 */
public class EditCountryActivity extends CountryInputActivity
{

    @Override
    @SuppressWarnings("all")
    protected void onCreate(Bundle savedInstanceState)
    {
        setContentView(R.layout.activity_edit_country);
        btnAction = (Button)findViewById(R.id.btnSave);
        super.onCreate(savedInstanceState);
        fillCurrentFormValues();
    }

    @Override
    protected boolean performAction(String country, String capital, long population, long area, String officialLanguages, byte[] flag)
    {
        String oldCountry = getIntent().getExtras().getString(COL_COUNTRY);
        return CountryApplication.getInstance().getCountryList().editCountry(oldCountry, country, capital, population, area, officialLanguages, flag);
    }

    /**
     * Fills in the form with the information already stored about the particular country chosen
     */
    @SuppressWarnings("all")
    private void fillCurrentFormValues()
    {
        etCountryValue.setText(getIntent().getExtras().getString(COL_COUNTRY));
        etCapitalValue.setText(getIntent().getExtras().getString(COL_CAPITAL));
        etPopulationValue.setText(String.valueOf(getIntent().getExtras().getLong(COL_POPULATION)));
        etAreaValue.setText(String.valueOf(getIntent().getExtras().getLong(COL_AREA)));
        etOfficialLanguagesValue.setText(getIntent().getExtras().getString(COL_OFFICIAL_LANGUAGES));
        byte[] flagBytes = getIntent().getExtras().getByteArray(COL_FLAG);
        Bitmap flag = BitmapFactory.decodeByteArray(flagBytes, 0, flagBytes.length);
        ivFlag.setImageBitmap(flag);
        ivFlag.setVisibility(flag == null ? View.GONE : View.VISIBLE);
        tvNoFlag.setVisibility(flag == null ? View.VISIBLE : View.GONE);
        btnRemoveFlag.setVisibility(flag == null ? View.GONE : View.VISIBLE);
    }

}
