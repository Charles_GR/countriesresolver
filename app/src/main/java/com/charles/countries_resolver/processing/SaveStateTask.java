package com.charles.countries_resolver.processing;

import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import com.charles.countries_resolver.data.CountryApplication;
import java.util.Set;

/**
 * Allows objects to be constructed which save the state of the user interface to file on a background thread
 */
public class SaveStateTask extends AsyncTask<Object, Void, Void>
{

    @Override
    @SuppressWarnings("unchecked")
    protected Void doInBackground(Object... params)
    {
        Editor editor = PreferenceManager.getDefaultSharedPreferences(CountryApplication.getInstance()).edit();
        editor.putString("SortByField", (String)params[0]);
        editor.putString("SortByOrdering", (String)params[1]);
        editor.putStringSet("CheckedCountries", (Set<String>)params[2]);
        editor.putInt("ListScrollIndex", (int)params[3]);
        editor.putInt("ListScrollTop", (int)params[4]);
        editor.apply();
        return null;
    }

}
