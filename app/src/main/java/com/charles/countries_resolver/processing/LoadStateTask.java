package com.charles.countries_resolver.processing;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import com.charles.countries_resolver.data.CountryApplication;
import com.charles.countries_resolver.view.CountryListActivity;
import java.util.HashSet;
import java.util.Set;

/**
 * Allows objects to be constructed which load the state of the user interface from file on a background thread
 */
public class LoadStateTask extends AsyncTask<Void, Void, Object[]>
{

    @Override
    protected Object[] doInBackground(Void... params)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(CountryApplication.getInstance());
        String sortByField = preferences.getString("SortByField", "Insertion");
        String SortByOrdering = preferences.getString("SortByOrdering", "Ascending");
        Set<String> checkedCountries = preferences.getStringSet("CheckedCountries", new HashSet<String>());
        int listScrollIndex = preferences.getInt("ListScrollIndex", 0);
        int listScrollTop = preferences.getInt("ListScrollTop", 0);
        return new Object[]{sortByField, SortByOrdering, checkedCountries, listScrollIndex, listScrollTop};
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void onPostExecute(Object... params)
    {
        super.onPostExecute(params);
        CountryListActivity.getInstance().updateState((String)params[0], (String)params[1], (Set<String>)params[2], (int)params[3], (int)params[4]);
    }

}
